<?php

$this->title = 'Suscription App';
?>

<div class="site-index">

    <div class="container"></div><div class="container">

        <div class="stepwizard col-md-offset-3">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                    <p>Personal Information</p>
                </div>
                <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Address Information</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>Payment Information</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>Review Information</p>
                </div>
            </div>
        </div>

        <form role="form" action="/" method="post">
            <div class="row setup-content" id="step-1">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Personal Information</h3>
                        <div class="form-group">
                            <label class="control-label">First Name</label>
                            <input id="firstname" maxlength="100" required="required" class="form-control" placeholder="Enter First Name" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input id="lastname" maxlength="100" required="required" class="form-control" placeholder="Enter Last Name" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telephone</label>
                            <input id="telephone" required="required" class="form-control" placeholder="Enter your telephone">
                        </div>
                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalAbort">Discard</button>
                        <button id="stepOneBtn" class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Address Information</h3>
                        <div class="form-group">
                            <label class="control-label">Main Street</label>
                            <input id="firststreet" maxlength="200" required="required" class="form-control" placeholder="Enter Street Name" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">House Number</label>
                            <input id="housenumber" maxlength="200" required="required" class="form-control" placeholder="Enter House Number" type="text">
                        </div>

                        <div class="form-group">
                            <label class="control-label">House</label>
                            <input id="housedesc" maxlength="200" required="required" class="form-control" placeholder="Enter House Desciption" type="text">
                        </div>

                        <div class="form-group">
                            <label class="control-label">City</label>
                            <input id="city" maxlength="200" required="required" class="form-control" placeholder="Enter City" type="text">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Zipcode</label>
                            <input id="zipcode" maxlength="200" required="required" class="form-control" placeholder="Enter Zipcode" type="text">
                        </div>

                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalAbort">Discard</button>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button id="stepTwoBtn" class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Payment Information</h3>
                        <div class="form-group">
                            <label class="control-label">Account Owner</label>
                            <input id="accountowner" maxlength="200" required="required" class="form-control" placeholder="Enter Account Owner" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">IBAN</label>
                            <input id="iban" maxlength="200" required="required" class="form-control" placeholder="Enter IBAN" type="text">
                        </div>

                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalAbort">Discard</button>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button id="fillInformationBtn" class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-4">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3>Preview Information</h3>

                        <dl class="dl-horizontal">
                            <dt>Firstname</dt>
                            <dd id="firstnameLt"></dd>

                            <dt>Lastname</dt>
                            <dd id="lastnameLt"></dd>

                            <dt>Telephone</dt>
                            <dd id="telephoneLt"></dd>

                            <dt>Primary Street</dt>
                            <dd id="primaryStLt"></dd>

                            <dt>House Number</dt>
                            <dd id="houseNumberLt"></dd>

                            <dt>House Description</dt>
                            <dd id="houseDescLt"></dd>

                            <dt>City</dt>
                            <dd id="cityLt"></dd>

                            <dt>Zip Code</dt>
                            <dd id="zipCodeLt"></dd>

                            <dt>Account Owner</dt>
                            <dd id="accountOwnerLt"></dd>

                            <dt>IBAN</dt>
                            <dd id="ibanLt"></dd>
                        </dl>
                        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalAbort">Discard</button>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button id="sendInformationBtn" class="btn btn-success btn-lg pull-right" type="button">Finish</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div id="modalAbort" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Discard Information?</h4>
            </div>
            <div class="modal-body">
                <p> Are you sure you want to discard this information? </p>
            </div>
            <div class="modal-footer">
                <button id="abortBtn" class="btn btn-success" data-dismiss="modal" type="button">Accept</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="modalInformation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Information</h4>
            </div>
            <div class="modal-body">
                <p id="informationData"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" onclick="location.href='/'" type="button">Go To Home</button>
            </div>
        </div>
    </div>
</div>

<div id="modalGetData" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 1250px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Information</h4>
            </div>
            <div class="modal-body">
                <p> It has been detected that there is already a registration process, would you like to continue? </p>
            </div>
            <div class="modal-footer">
                <button id="recoverBtn" class="btn btn-success" data-dismiss="modal" type="button">Si</button>
                <button id="clearBtn" type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
