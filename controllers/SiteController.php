<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Payment;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionManageData(){
        if(isset(Yii::$app->request->isAjax)){

            $url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
            $customer = new Customer();

            $customer->firstname = $_POST['firstname'];
            $customer->lastname = $_POST['lastname'];
            $customer->telephone = $_POST['telephone'];
            $customer->city = $_POST['city'];
            $customer->house = $_POST['homeDesc'];
            $customer->number = $_POST['homeNum'];
            $customer->zipcode = $_POST['zipCode'];
            $customer->primarystreet = $_POST['primarySt'];
            $customer->iban = $_POST['iban'];
            $customer->accountowner = $_POST['accountOwner'];

            if($customer->save()){

                $dataPayment = [
                    "customerId" => $customer->id,
                    "iban" => $_POST['iban'],
                    "owner"=> $_POST['accountOwner']
                ];

                $paymentCall = curl_init($url);
                curl_setopt($paymentCall,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($paymentCall, CURLOPT_POSTFIELDS, json_encode($dataPayment));
                $response = curl_exec($paymentCall);

                // close the connection, release resources used
                curl_close($paymentCall);

                $paymentId = substr($response,18, 96);

                if($paymentId){
                    Yii::$app->db->createCommand()
                        ->update('customer', ['paymentid' => $paymentId], ['id' => $customer->id])
                        ->execute();

                    return $paymentId;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
}
