<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $telephone
 * @property string $primarystreet
 * @property string $number
 * @property string $house
 * @property string $city
 * @property string $zipcode
 * @property string $accountowner
 * @property string $iban
 * @property string $paymentid
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'telephone', 'primarystreet', 'number', 'house', 'city', 'zipcode', 'accountowner', 'iban'], 'required'],
            [['firstname', 'lastname', 'house'], 'string', 'max' => 45],
            [['telephone'], 'string', 'max' => 20],
            [['primarystreet', 'city', 'accountowner'], 'string', 'max' => 90],
            [['number'], 'string', 'max' => 10],
            [['zipcode'], 'string', 'max' => 15],
            [['iban'], 'string', 'max' => 25],
            [['paymentid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'telephone' => 'Telephone',
            'primarystreet' => 'Primarystreet',
            'number' => 'Number',
            'house' => 'House',
            'city' => 'City',
            'zipcode' => 'Zipcode',
            'accountowner' => 'Accountowner',
            'iban' => 'Iban',
            'paymentid' => 'Paymentid',
        ];
    }
}
