$(function () {
    //Elements
    $baseUrl = $('#baseUrl');
    $firstName = $('#firstname');
    $lastName = $('#lastname');
    $telephone = $('#telephone');
    $primarySt = $('#firststreet');
    $homeNum = $('#housenumber');
    $houseDesc = $('#housedesc');
    $city = $('#city');
    $zipCode = $('#zipcode');
    $accountOwner = $('#accountowner');
    $iban = $("#iban");

    $sendInfoBtn = $('#sendInformationBtn');
    $stepOneBtn = $('#stepOneBtn');
    $stepTwoBtn = $('#stepTwoBtn');
    $recoverBtn = $('#recoverBtn');
    $clearBtn = $('#clearBtn');

    $fillInformationBtn = $('#fillInformationBtn');
    $abortBtn = $("#abortBtn");

    if(localStorage.getItem('stepNumber')){

        $('#modalGetData').modal(); //Open modal

        $recoverBtn.click(function () {
            //local storage get's
            if(localStorage.getItem('stepNumber') == 1){
                $firstName.val(localStorage.getItem("firstName"));
                $lastName.val(localStorage.getItem("lastName"));
                $telephone.val(localStorage.getItem("telephone"));
            }else if(localStorage.getItem('stepNumber') == 2){
                $nextStepWizard = $('div.setup-panel div a[href="#' + 'step-1' + '"]').parent().next().children("a");
                $nextStepWizard.removeAttr('disabled').trigger('click');
                $firstName.val(localStorage.getItem("firstName"));
                $lastName.val(localStorage.getItem("lastName"));
                $telephone.val(localStorage.getItem("telephone"));
                $primarySt.val(localStorage.getItem("primarySt"));
                $homeNum.val(localStorage.getItem("homeNum"));
                $houseDesc.val(localStorage.getItem("houseDesc"));
                $city .val(localStorage.getItem("city"));
                $zipCode.val(localStorage.getItem("zipCode"));
            }else if(localStorage.getItem('stepNumber') == 3){
                $nextStepWizard = $('div.setup-panel div a[href="#' + 'step-2' + '"]').parent().next().children("a");
                $nextStepWizard.removeAttr('disabled').trigger('click');
                $firstName.val(localStorage.getItem("firstName"));
                $lastName.val(localStorage.getItem("lastName"));
                $telephone.val(localStorage.getItem("telephone"));
                $primarySt.val(localStorage.getItem("primarySt"));
                $homeNum.val(localStorage.getItem("homeNum"));
                $houseDesc.val(localStorage.getItem("houseDesc"));
                $city .val(localStorage.getItem("city"));
                $zipCode.val(localStorage.getItem("zipCode"));
                $accountOwner.val(localStorage.getItem("accountOwner"));
                $iban.val(localStorage.getItem("iban"));
            }else{
                $nextStepWizard = $('div.setup-panel div a[href="#' + 'step-3' + '"]').parent().next().children("a");
                $nextStepWizard.removeAttr('disabled').trigger('click');
                $nextStepWizard = $('div.setup-panel div a[href="#' + 'step-2' + '"]').parent().next().children("a");
                $nextStepWizard.removeAttr('disabled').trigger('click');
                $firstName.val(localStorage.getItem("firstName"));
                $lastName.val(localStorage.getItem("lastName"));
                $telephone.val(localStorage.getItem("telephone"));
                $primarySt.val(localStorage.getItem("primarySt"));
                $homeNum.val(localStorage.getItem("homeNum"));
                $houseDesc.val(localStorage.getItem("houseDesc"));
                $city .val(localStorage.getItem("city"));
                $zipCode.val(localStorage.getItem("zipCode"));
                $accountOwner.val(localStorage.getItem("accountOwner"));
                $iban.val(localStorage.getItem("iban"));
                $('#firstnameLt').html(localStorage.getItem("firstName"));
                $('#lastnameLt').html(localStorage.getItem("lastName"));
                $('#telephoneLt').html(localStorage.getItem("telephone"));
                $('#primaryStLt').html(localStorage.getItem("primarySt"));
                $('#houseNumberLt').html(localStorage.getItem("homeNum"));
                $('#houseDescLt').html(localStorage.getItem("houseDesc"));
                $('#cityLt').html(localStorage.getItem("city"));
                $('#zipCodeLt').html(localStorage.getItem("zipCode"));
                $('#accountOwnerLt').html(localStorage.getItem("accountOwner"));
                $('#ibanLt').html(localStorage.getItem("iban"));
            }
        })

        $clearBtn.click(function () {
            localStorage.clear(); //clear local storage
        });
    }

    $sendInfoBtn.click(function () {
        //call sendInformation function
        sendInformation($firstName.val(), $lastName.val(), $telephone.val(), $primarySt.val(), $homeNum.val(),
                        $houseDesc.val(),$city.val(), $zipCode.val(), $accountOwner.val(), $iban.val())
    });

    $stepOneBtn.click(function () {
        if($firstName.val() != '' && $lastName.val() != '' && $telephone.val() != ''){
            localStorage.setItem('firstName', $firstName.val());
            localStorage.setItem('lastName', $lastName.val());
            localStorage.setItem('telephone', $telephone.val());
            localStorage.setItem('stepNumber', 2);
        }
    });

    $stepTwoBtn.click(function () {
        if($primarySt.val() != '' && $homeNum.val() != '' && $houseDesc.val() != '' && $city.val() != '' && $zipCode.val() != ''){
            localStorage.setItem('primarySt', $primarySt.val());
            localStorage.setItem('homeNum', $homeNum.val());
            localStorage.setItem('houseDesc', $houseDesc.val());
            localStorage.setItem('city', $city.val());
            localStorage.setItem('zipCode', $zipCode.val());
            localStorage.setItem('stepNumber', 3);
        }
    });

    $fillInformationBtn.click(function () {
        $('#firstnameLt').html($firstName.val());
        $('#lastnameLt').html($lastName.val());
        $('#telephoneLt').html($telephone.val());
        $('#primaryStLt').html($primarySt.val());
        $('#houseNumberLt').html($homeNum.val());
        $('#houseDescLt').html($houseDesc.val());
        $('#cityLt').html($city.val());
        $('#zipCodeLt').html($zipCode.val());
        $('#accountOwnerLt').html($accountOwner.val());
        $('#ibanLt').html($iban.val());

        if($accountOwner.val() != '' && $iban.val() != ''){
            localStorage.setItem('accountOwner', $accountOwner.val());
            localStorage.setItem('iban', $iban.val());
            localStorage.setItem('stepNumber', 4);
        }
    });
    
    $abortBtn.click(function () {
        localStorage.clear(); //clear local storage
        window.location.href = "/";
    });
});

function sendInformation(firstname, lastname, telephone, primarySt, homeNum, homeDesc, city, zipCode, accountOwner, iban) {
    $.post( $baseUrl.val() + '/site/manage-data', {
        firstname: firstname,
        lastname: lastname,
        telephone: telephone,
        primarySt: primarySt,
        homeNum: homeNum,
        homeDesc: homeDesc,
        city: city,
        zipCode: zipCode,
        accountOwner: accountOwner,
        iban: iban
    }).done(function (data) {
       localStorage.clear(); //clear local storage
       $('#modalInformation').modal('show');
       $('#informationData').html("Data saving successful <br> Payment ID: " + data);

    }).fail(function () {
       $('#modalInformation').modal();
       $('#informationData').html("Have occurred an error, try again later.")
    })
}